#!/usr/bin/python3

from random import randint
import time

rock = 1
paper = 2
scissors = 3

# Create Dictionaries for the reference the game names and results
names = {rock: 'Rock', paper: 'Paper', scissors: 'Scissors'}
rules = {rock: scissors, paper: rock, scissors: paper}

# Used to calculate the running score of game results
player_score = 0
computer_score = 0


# Starts the Game with an introduction
def start():
    print('Let\'s play a game of rock, paper, scissors')
    while game():
        pass
    scores()


# Defines the game play, the player input and the random generated
# input by the computer.
# It gets the results and prompts the play_again() function
def game():
    player = move()
    computer = randint(1, 3)
    result(player, computer)

    return play_again()


# Rules of the game and parameters, throws error if unknown player input is used
def move():
    while True:
        player = input('Rock = 1\nPaper = 2\nScissors = 3\nMake a move: ')
        try:
            player = int(player)
            if player in (1, 2, 3):
                return player
        except ValueError:
            pass
        print('Oops! I didn\'t understand that. Please enter 1, 2, or 3!')


# Results of the player pick vs the computer pick
def result(player, computer):
    print('1...')
    time.sleep(1)
    print('2...')
    time.sleep(1)
    print('3!')
    time.sleep(.5)

    print('Computer threw {0}!\n\n'.format(names[computer]))
    global player_score, computer_score

    if player == computer:
        print('Tie Game!')
    else:
        if rules[player] == computer:
            print('Your victory has been assured!')
            player_score += 1
        else:
            print('The computer laughs as you realize you have been defeated!')
            computer_score += 1


# At end of the game, asks if you would like to play again
def play_again():
    answer = input('Would you like to play again? (y/n): ')

    if answer in ('y', 'Y', 'yes', 'Yes', 'of course!'):
        return answer
    else:
        print('Thank you for playing rock, paper, scissors!')


# Adds up the total running score after single game or if choose to
# play multiple games
def scores():
    global player_score, computer_score
    print('HIGH SCORES')
    print('Player: ', player_score)
    print('Computer: ', computer_score)


if __name__ == '__main__':
    start()
