from collections import namedtuple
from pygame import *
from pygame.examples.headless_no_windows_needed import screen as pgs

W = 800
H = 600
RED = 200, 0, 0
WHITE = 200, 200, 200

bat = Rect((W / 2, 0.96 * H), (150, 15))
ball = Rect((W / 2, H / 2), (30, 30))
Direction = namedtuple('Direction', 'x y')
ball_dir = Direction(5, -5)


def draw():
    pgs.clear()
    pgs.draw.filled_rect(bat, RED)
    pgs.draw.filled_rect(ball, WHITE)


def on_mouse_move(pos):
    x, y = pos
    bat.center = (x, bat.center[1])


def on_mouse_down():
    global ball_dir
    ball_dir = Direction(ball_dir.x * 1.5, ball_dir.y * 1.5)


def move(ball):
    global ball_dir
    ball.move_ip(ball_dir)


def update():
    move(ball)
