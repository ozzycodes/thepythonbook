import sys
import time

from collections import namedtuple
from pygame import *
from pygame.examples.headless_no_windows_needed import screen as pgs
from pygame.rect import Rect

W = 804
H = 600
RED = 200, 0, 0
WHITE = 200, 200, 200
GOLD = 205, 145, 0

ball = Rect((W / 2, H / 2), (30, 30))
Direction = namedtuple('Direction', 'x y')
ball_dir = Direction(5, -5)

bat = Rect((W / 2, 0.96 * H), (120, 15))


class Block(Rect):

    def __init__(self, colour, rect):
        """
        Return Type for init object
        :rtype: object
        """
        Rect.__init__(self, rect)
        self.colour = colour


blocks = []
for n_block in range(24):
    block = Block(GOLD, ((((n_block % 8) * 100) + 2, ((n_block // 8) * 25) + 2), (96, 23)))
    blocks.append(block)


def draw_blocks():
    for block in blocks:
        pgs.draw.filled_rect(block, block.colour)


def draw():
    pgs.clear()
    pgs.draw.filled_rect(ball, WHITE)
    pgs.draw.filled_rect(bat, RED)
    draw_blocks()


def on_mouse_move(pos):
    x, y = pos
    bat.center = (x, bat.center[1])


def on_mouse_down():
    global ball_dir
    ball_dir = Direction(ball_dir.x * 1.5, ball_dir.y * 1.5)


def move(ball):
    global ball_dir
    ball.move_ip(ball_dir)

    if ball.x > 781 or ball.x <= 0:
        ball_dir = Direction(-1 * ball_dir.x, ball_dir.y)

    if ball.y <= 0:
        ball_dir = Direction(ball_dir.x, abs(ball_dir.y))

    if ball.colliderect(bat):
        pygame.mixer.play('blip.wav')
        ball_dir = Direction(ball_dir.x, - abs(ball_dir.y))

    to_kill = ball.collidelist(blocks)

    if to_kill >= 0:
        pygame.mixer.play('block.wav')
        ball_dir = Direction(ball_dir.x, abs(ball_dir.y))
        blocks.pop(to_kill)

    if not blocks:
        pygame.mixer.play('win.wav')
        pygame.mixer.play('win.wav')
        print("Winner!")
        time.sleep(1)
        sys.exit()

    if ball.y > H:
        pygame.mixer.play('die.wav')
        print("Loser!")
        time.sleep(1)
        sys.exit()


def update():
    move(ball)
