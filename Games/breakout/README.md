You need: PyGame Zero (pg0) - http://pygame-zero.readthedocs.org/

If you have a Raspberry Pi running Raspbian Jessie, you have pg0 installed.

The sounds/ subdirectory must be in the same directory as breakout.py
Run:
pgzrun breakout.py

This code was published as part of a six page introduction to pg0 featured in Linux User & Developer magazine #159.
It is licensed under the MIT licence - see LICENCE file, attached - and is intended to encourage trying programming with pg0 - improvements and dissemination are encouraged.


