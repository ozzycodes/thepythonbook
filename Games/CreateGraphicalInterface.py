#!/usr/bin/python3

from tkinter import *
from Games import RockPaperScissor_GUI, Rock_Paper_Scissors, HangMan, LinuxDicePoker

root = Tk()
root.title('Linux User & Developer Game Collection')

mainframe = Frame(root, height=200, width=500)
mainframe.pack_propagate(0)
mainframe.pack(padx=5, pady=5)

intro = Label(mainframe, text="Welcome to OzzyCodes Micro-games Collection. Please select one of "
                              "the following game to play!: ")
intro.pack(side=TOP)

rpsGUI_button = Button(mainframe, text='Rock, Paper, Scissors GUI', command=RockPaperScissor_GUI.gui())
rpsGUI_button.pack()

rps_button = Button(mainframe, text='Rock, Paper, Scissors', command=Rock_Paper_Scissors.start())
rps_button.pack()

hm_button = Button(mainframe, text='Hangman', command=HangMan.start())
hm_button.pack()

ldp_button = Button(mainframe, text='Linux Dice Poker', command=LinuxDicePoker.start())
ldp_button.pack()

exit_button = Button(mainframe, text='Quit', command=root.destroy())
exit_button.pack(side=BOTTOM)

root.mainloop()
