import sys
from snack import *

from grub import (readBootDB, writeBootFile)


def main(entry_value='1'):
    global c, li
    try:
        (default_value, entry_value, kernels) = readBootDB()
    except:
        print( >> sys.stderr, "Error reading /boot/grub/grub.conf.")
        sys.exit(10)

    screen = SnackScreen()

    while True:
        g = GridForm(screen, "Boot configuration", 1, 5)
        if len(kernels) > 0:
            li = Listbox(height=len(kernels), width=20, returnExit=1)
            for i, x in enumerate(kernels):
                li.append(x, i)
            g.add(li, 0, 0)
            li.setCurrent(default_value)

        bb = ButtonBar(screen, ("Ok", "ok"), "Cancel", "cancel")

        e = Entry(3, str(entry_value))
        l = Label("Timeout (in seconds):")
        gg = Grid(2, 1)
        gg.setField(l, 0, 0)
        gg.setField(e, 1, 0)

        g.add(Label(''), 0, 1)
        g.add(gg, 0, 2)
        g.add(Label(''), 0, 3)
        g.add(bb, 0, 4, growx=1)
        result = g.runOnce()
        if bb.buttonPressed(result) == 'cancel':
            screen.finish()
            sys.exit(0)
        else:
            entry_value = e.value()
            try:
                c = int(entry_value)
                break
            except ValueError:
                continue

    writeBootFile(c, li.current())
    screen.finish()


if __name__ == '__main__':
    main()
