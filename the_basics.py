#!/usr/bin/python3

hello_str = 'Hello, world!'
hello_int = 21
hello_bool = True
hello_tuple = (21, 32)
hello_list = ["Hello,", "this", "is", "a", "list!"]

#  This list contains 5 strings. The list has no spaces
#  between the list items so will be joined together


# Starts out with an empty list
hello_list = list()
hello_list.append("Hello,")
hello_list.append("this")
hello_list.append("is")
hello_list.append("a")
hello_list.append("list!")

hello_dict = {'first_name': 'Phil',
              'last_name': 'Broccoli',
              'eye_color': 'Hazel'}

print(hello_list[4])
hello_list[4] += '!'

print(hello_list[4])

print(hello_tuple[0])

print(hello_dict['first_name'] + "" + hello_dict['last_name'] +
      "has" + hello_dict['eye_color'] + "eyes")

print("{0} {1} has {2} eyes.".format(hello_dict['first_name'],
                                     hello_dict['last_name'],
                                     hello_dict['eye_color']))
